# myBudget General Requirements
A loose list of requirements for this application. 

## Core Functionality
- Be accessible from desktop and mobile
- Set static values like income, and monthly bills
	- In turn, be able to add monthly bills or edit these values without changing previous monthly budgets

- Easily add purchases made during the month
	- Be able to easily view the purchases by category, and to define the categories.
	- Be able to edit any of the values 
	
- Be able to view the totals for each month. Some sort of stats that would provide additional info (ex: percentage under budget)

## Tables
The following are the initial plans for the tables. Values that aren't required are italicized. Primary key is default int unless otherwise stated.
	
**Monthly Income**
- Value(decimal), Start(date), *End(date)*
	
**Monthly Bills**
- Name(string), Cost(decimal), Start(date), *End(date)*

**Purchases**
- Category(join PK), Cost(decimal), PurchaseDate(date), DateEntered(date), *Notes(text)*

**Categories**
- Name(string, unique)


## Specific Features 
The following are a list of specific features that cover specific details.

**Monthly Bills**
- Perhaps have the ability to set the Cost to be the percentage of monthly income. And maybe be able to see what the percentage of monthly income the cost is. 
- When monthly bills have their cost updated, the old version of the bill should be given an End date, and a new entry is added with the same name and new cost that will be used going forward. 

**Purchases**
- Be able to create a new category while adding a new purchase. But also be able to easily select from previous categories.