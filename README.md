# myBudget

## Project Status: CANCELLED
This project has been shelved in favour of a project that better aligns with my goals. This has been left up and made available for viewing to demonstrate my decision-making process. For more information, see the [Wiki](https://gitlab.com/Known_Stranger/mybudget/wikis/home).

## Description
A simple website that allows for the user to set a monthly budget, tracking both static and non-static expenses (bills and purchases). Monthly bills can be set, and any other purchases can be entered as they come up.

## Purpose
To assist the user in tracking expenses and managing a budget on a month to month basis. The application should allow for monthly bills to automatically be accounted for, and to easily add any other purchases made.

## License
Currently this is a personal project with no plan to distribute it. Should that change a license will be provided.
