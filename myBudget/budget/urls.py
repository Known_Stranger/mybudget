from django.urls import path

from budget.views import Home

urlpatterns = [
    path('', Home.as_view()),
]
